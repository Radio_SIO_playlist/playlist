#include <iostream>
#include <string>
#include "headers/Playlist.hpp"

namespace playgen {
	/// Constructor
	Playlist::Playlist(unsigned int id, std::string name, std::string args) :
					id(id), name(name), args(args) { }
	/// Getter
	unsigned int Playlist::getId() const { return id; }
	std::string Playlist::getName() const { return name; }
	std::string Playlist::getArgs() const { return args; }
	/// Setter
	void Playlist::setId(unsigned int id) { this->id = id; }
	void Playlist::setName(std::string name) { this->name = name; }
	void Playlist::setArgs(std::string args) { this->args = args; }
	/// Public fonction
	void Playlist::print() const {
		std::cout
			<< "(Playlist : " << id << "; " << name << "; "<< args << ")" <<
		std::endl;
	}
}
