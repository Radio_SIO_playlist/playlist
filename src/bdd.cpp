#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "headers/libpq/libpq-fe.h"
#include <regex>
#include <sstream>
#include <vector>

extern std::string playlist_brute;
extern std::vector<std::string> split(std::string, char);
extern std::string dbPass, dbHost, dbName, dbPort, dbSchema, dbLogin, dbFileConfDelimiter;



/* =========================== */
// Requete A la Base de donnée //
/* =========================== */
/*https://momjian.us/main/writings/pgsql/aw_pgsql_book/node146.html#libpq_statename_application*/
void requeteToBDD(char SQL_request_String[256]){
	if (dbHost != "" || dbPort != "" || dbName != "" || dbLogin != ""|| dbPass != "")
    {
        char query_string[256]; /* holds constructed SQL query */
    	PGconn *connection; /* holds database connection */
    	PGresult * result; /* holds query results */
    	int i,j;

    	connection = PQsetdbLogin(
        dbHost.c_str(),
        dbPort.c_str(),
        "",
        "",
        dbName.c_str(),
        dbLogin.c_str(),
        dbPass.c_str());

    	/* did the database connection fail? */
    	if (PQstatus(connection) == CONNECTION_BAD){
    		fprintf(stderr, "Connection to database failed.\n");
    		fprintf(stderr, "%s\n", PQerrorMessage(connection));
    		exit(1);
    	}

    	/* create an SQL query string */
    	sprintf(query_string, SQL_request_String);

    	/* send the query */
    	result = PQexec(connection, query_string);

    	/* did the query fail? */
    	if (PQresultStatus(result) != PGRES_TUPLES_OK) {
        std::cout << "Ah... Something went wrong... :'( : " << PQerrorMessage(connection) << std::endl;
    		PQclear(result);
    		PQfinish(connection);
    		exit(1);
    	}

    	/* loop trought all rows returned */
    	for (i=0; i<PQntuples(result);i++){
        for (j=0; j<PQnfields(result);j++){
            playlist_brute += PQgetvalue(result, i , j) + std::string("|");
        }
        playlist_brute += "\n";
	    }

    	/* free result */
    	PQclear(result);

	    /* disconnect from the database */
	    PQfinish(connection);
    }
    else
    {
        std::cout << "You don't have configured your configuration file ! See ~/.config/playgen/bdd.conf" << std::endl;
    }
}
