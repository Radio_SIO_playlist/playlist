#ifndef ARTIST_HPP
#define ARTIST_HPP
#include <string>

namespace playgen {
	/// Class Artist
	class Artist {
		private: /// Private data members
			unsigned int id;
			std::string name;
			std::string surname;
		public:
			/// Constructors - Default
			Artist(unsigned int id = 0, std::string name = "name", std::string surname = "surname");
			/// Getters
			unsigned int getId() const;
			std::string getName() const;
			std::string getSurname() const;
			/// Setters
			void setId(unsigned int);
			void setName(std::string);
			void setSurname(std::string);
			void print() const; /// print function to show the Artist



	};
}
#endif
