#ifndef ARG_H
#define ARG_H
#include <argp.h>
#include "bdd.hpp"



/* Used by main to communicate with parse_opt. */
struct argument {
  char *args[2];                /* arg1 & arg2 */
  int silent, verbose;
  char *duration;
  char *genre;
  char *type;
  char *album;
  char *artist;
  char *format;
};

const char *argp_program_version =
  "PlayGen 0.1";
const char *argp_program_bug_address =
  "<contact@playgen.com>";

/* Program documentation. */
static char doc[] =
  "PlayGen 0.1 -- a little program can generate a personalized playlist";

/* A description of the arguments we accept. */
static char args_doc[] = "<playlist name>";

/* The options we understand. */
static struct argp_option options[] = {
  {"verbose",  'v', 0,      0,  "Produce verbose output" },
  {"quiet",    'q', 0,      0,  "Don't produce any output" },
  {"silent",   's', 0,      OPTION_ALIAS },
  {"duration", 'd', "<int>", 0, "Duration of the playlist asked"},
  {"genre", 'g', "<name>", 0, "Genre of the playlist asked"},
  {"ugenre", 'u', "<playlist type>", 0, "Type of the playlist output."},
  {"type", 't', "<playlist type>", 0, "Type of the playlist output."},
  {"album", 'l', "<album name>", 0, "Name of the album selected."},
  {"artist", 'a', "<artist name>", 0, "Name of the artist selected."},
  {"format", 'f', "<mp3>", 0, "Format of the track file."},
  { 0 }
};
/* Parse a single option. */
static error_t parse_opt (int key, char *arg, struct argp_state *state) {
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  struct argument *arguments = ( struct argument *)state->input;

  switch (key) {
    case 'q': case 's':
      arguments->silent = 1;
      break;
    case 'v':
      arguments->verbose = 1;
      break;
    case 'd':
      arguments->duration = arg;
      break;
    case 'g':
      arguments->genre = arg;
      break;
    case 't':
      arguments->type = arg;
      break;
    case 'l':
      arguments->album = arg;
      break;
    case 'a':
      arguments->artist = arg;
      break;
    case 'f':
      arguments->format = arg;
      break;

    case ARGP_KEY_ARG:
      if (state->arg_num > 0)
        /* Too many arguments. */
        argp_usage (state);

      arguments->args[state->arg_num] = arg;

      break;

    case ARGP_KEY_END:
      if (state->arg_num < 1)
        /* Not enough arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };
static struct argument *arguments = new argument();

void argfi(int argc, char **argv){

  /* Default values. */
  arguments->silent = 0;
  arguments->verbose = 0;
  arguments->duration = "";
  arguments->genre = "";
  arguments->type = "";
  arguments->album = "";
  arguments->artist = "";
  arguments->format = "";

  /* Parse our arguments; every option seen by parse_opt will
     be reflected in arguments. */
  argp_parse (&argp, argc, argv, 0, 0, arguments);

  printf("GENRE = %s\nDURATION = %s\n",arguments->genre,arguments->duration );
  printf("TYPE = %s\nALBUM = %s\n", arguments->type, arguments->album);
  printf("ARTIST = %s\nFORMAT = %s\n", arguments->artist,arguments->format);
  printf("VERBOSE = %s\nSILENT = %s\n", arguments->verbose, arguments->silent);

}

#endif
