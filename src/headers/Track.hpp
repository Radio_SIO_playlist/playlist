#include <string>

namespace playgen {

	/// Class Track
	class Track {
		protected:
			int id;
			std::string title;
			artist id;
			album id;
			genre id;
			int duration;
			std::string format;
			std::string path;

		public:
			/// Methods
			void SetFormat(const std::string& format) {this->format = format;}
			void SetId(const genre& id) {this->id = id;}
			void SetPath(const std::string& path) {this->path = path;}
			void SetTitle(const std::string& title) {this->title = title;}
			void SetDuration(int duration) {this->duration = duration;}
			int  GetDuration() const {return duration;}
			const std::string& GetFormat() const {return format;}
			const genre& GetId() const {return id;}
			const std::string& GetPath() const {return path;}
			const std::string& GetTitle() const {return title;}

			/// Constructors
			Track(void); /// Default constructor
			Track(unsigned int, std::string, artist, album, genre, int, std::string, std::string); /// Constructor of Track
			~Track(void); /// Desctructor

	};
}
