#ifndef GLOBAL_H
#define GLOBAL_H
#include <string>
#include <vector>

std::string playlist_brute;
std::string path_file;
std::vector<std::string> path_file_list;
std::string dbPass, dbHost, dbName, dbPort, dbSchema, dbLogin, dbFileConfDelimiter;
void recupConfBdd();

#endif
