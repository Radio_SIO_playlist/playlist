#ifndef Xspf_HPP
#define Xspf_HPP
#include <string>
#include "Playlist.hpp"

namespace playgen {
	/// Class Xspf child of Playlist
	class Xspf : public Playlist {
		public:
			/// Constructors - Default
			Xspf(unsigned int id = 0, std::string name = "name", std::string args = "args");
	};
}
#endif
