#ifndef Genre_HPP
#define Genre_HPP
#include <string>

namespace playgen {
	/// Class Genre
	class Genre {
		private: /// Private data members
			unsigned int id;
			std::string name;
		public:
			/// Constructors - Default
			Genre(unsigned int id = 0, std::string name = "name");
			/// Getters
			unsigned int getId() const;
			std::string getName() const;
			/// Setters
			void setId(unsigned int);
			void setName(std::string);
			void print() const; /// print function to show the Genre
	};
}
#endif
