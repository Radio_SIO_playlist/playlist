#ifndef ALBUM_HPP
#define ALBUM_HPP
#include <string>

namespace playgen {
	/// Class Album
	class Album {
		private: /// Private data members
			unsigned int id;
			std::string name;
		public:
			/// Constructors - Default
			Album(unsigned int id = 0, std::string name = "name");
			/// Getters
			unsigned int getId() const;
			std::string getName() const;
			/// Setters
			void setId(unsigned int);
			void setName(std::string);
			void print() const; /// print function to show the Album
	};
}
#endif
