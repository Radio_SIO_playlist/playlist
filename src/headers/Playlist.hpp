#ifndef Playlist_HPP
#define Playlist_HPP
#include <string>

namespace playgen {
	/// Class Playlist
	class Playlist {
		private: /// Private data members
			unsigned int id;
			std::string name;
			std::string args;
		public:
			/// Constructors - Default
			Playlist(unsigned int id = 0, std::string name = "name", std::string args = "args");
			/// Getters
			unsigned int getId() const;
			std::string getName() const;
			std::string getArgs() const;
			/// Setters
			void setId(unsigned int);
			void setName(std::string);
			void setArgs(std::string);
			void print() const; /// print function to show the Playlist
	};
}
#endif
