#ifndef M3u_HPP
#define M3u_HPP
#include <string>
#include "Playlist.hpp"

namespace playgen {
	/// Class M3u child of Playlist
	class M3u : public Playlist {
		public:
			/// Constructors - Default
			M3u(unsigned int id = 0, std::string name = "name", std::string args = "args");
	};
}
#endif
