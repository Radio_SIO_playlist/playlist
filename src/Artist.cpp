#include <iostream>
#include <string>
#include "headers/Artist.hpp"

namespace playgen {
    /// Constructor
    Artist::Artist(unsigned int id, std::string name, std::string surname) :
                    id(id), name(name), surname(surname) { }
    /// Getter
    unsigned int Artist::getId() const { return id; }
    std::string Artist::getName() const { return name; }
    std::string Artist::getSurname() const { return surname; }
    /// Setter
    void Artist::setId(unsigned int id) { this->id = id; }
    void Artist::setName(std::string name) { this->name = name; }
    void Artist::setSurname(std::string surname) { this->surname = surname; }
    /// Public fonction
    void Artist::print() const {
      std::cout << "(Artist : " << id << "; " << name << "; " << surname << ")" << std::endl;
    }
}
