#include <iostream>
#include <string>
#include "headers/Genre.hpp"

namespace playgen {
	/// Constructor
	Genre::Genre(unsigned int id, std::string name) :
					id(id), name(name) { }
	/// Getter
	unsigned int Genre::getId() const { return id; }
	std::string Genre::getName() const { return name; }
	/// Setter
	void Genre::setId(unsigned int id) { this->id = id; }
	void Genre::setName(std::string name) { this->name = name; }
	/// Public fonction
	void Genre::print() const {
		std::cout << "(Genre : " << id << "; " << name << ")" << std::endl;
	}
}
