#include <string>
#include <vector>
#include <sstream>
#include <regex>
#include <string>
#include <iostream>
#include <fstream>


extern std::vector<std::string> split(std::string, char);
extern std::string dbPass, dbHost, dbName, dbPort, dbSchema, dbLogin, dbFileConfDelimiter;

/* ================= */
//  Split function   //
/* ================= */
std::vector<std::string> split(std::string str, char delimiter) {
  std::vector<std::string> internal;
  std::stringstream ss(str); // Turn the string into a stream.
  std::string stringOutput;

  while(getline(ss, stringOutput, delimiter)) {
    internal.push_back(stringOutput);
  }

  return internal;
}


std::ifstream bddConfStream;


/* Le fichier bdd.conf doit avoir la structure suivante :
dbHost=
dbPort=
dbName=
bSchema=
dbLogin=
dbPass=
*/

std::string bddConfFile="/etc/playgen/bdd.conf";
/* ================= */
//   Read bdd.conf   //
/* ================= */
void recupConfBdd(){
	dbFileConfDelimiter="=";
	std::string line;
	std::ifstream bddConfStream (bddConfFile);

	if (bddConfStream.good() != 0){
		if (bddConfStream.is_open()){
			while (std::getline (bddConfStream,line)) {
				if(regex_match (line,std::regex ("^dbHost=.*"))){
					std::vector<std::string> confData = split(line, '=');
                    if ( confData.size() == 2 )
                    {
					    dbHost = confData[1];
                    }
                }
				if(regex_match (line,std::regex ("^dbPort=.*"))){
					std::vector<std::string> confData = split(line, '=');
                    if ( confData.size() == 2 )
                    {
					    dbPort = confData[1];
                    }
				}
				if(regex_match (line,std::regex ("^dbName=.*"))){
					std::vector<std::string> confData = split(line, '=');
                    if ( confData.size() == 2 )
                    {
					    dbName = confData[1];
                    }
				}
                if(regex_match (line,std::regex ("^dbLogin=.*"))){
					std::vector<std::string> confData = split(line, '=');
                    if ( confData.size() == 2 )
                    {
					    dbLogin = confData[1];
                    }
				}
				if(regex_match (line,std::regex ("^dbSchema=.*"))){
					std::vector<std::string> confData = split(line, '=');
                    if ( confData.size() == 2 )
                    {
					    dbSchema = confData[1];
                    }
				}
				if(regex_match (line,std::regex ("^dbPass=.*"))){
					std::vector<std::string> confData = split(line, '=');
                    if ( confData.size() == 2 )
                    {
					    dbPass = confData[1];
                    }
				}
			}
			bddConfStream.close();
		}
		else {
			std::cout << "Error ! File already open !";
		}
	}
	else {
		std::cout << "Error 0x2a ! Password file not found !";
	}
}
