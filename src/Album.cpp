#include <iostream>
#include <string>
#include "headers/Album.hpp"

namespace playgen {
	/// Constructor
	Album::Album(unsigned int id, std::string name) :
					id(id), name(name) { }
	/// Getter
	unsigned int Album::getId() const { return id; }
	std::string Album::getName() const { return name; }
	/// Setter
	void Album::setId(unsigned int id) { this->id = id; }
	void Album::setName(std::string name) { this->name = name; }
	/// Public fonction
	void Album::print() const {
		std::cout << "(Album : " << id << "; " << name << ")" << std::endl;
	}
}
