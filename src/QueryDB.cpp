#include <string.h>
#include <vector>
#include <iostream>
#include <stdio.h>

#include "headers/QueryDB.hpp"
#include "headers/arg.hpp"
#include "headers/global.hpp"

extern std::vector<std::string> split(std::string, char);

void search_db(int argc, char **argv)
{
    argfi(argc, argv);
    recupConfBdd();

    if (arguments->artist != "")
    {

        // On split le contenu de notre argument afin de savoir quel le pourcentage de chaque option demandé par l'user
        std::vector<std::string> artistData = split(arguments->artist, ',');
        for (int i=0; i < artistData.size(); i++)
        {
            std::vector<std::string> artistPourcentage = split(artistData[i], '=');
            std::cout << artistPourcentage[0];
            if (artistPourcentage.size() > 1)
            {
                std::cout << " - " << artistPourcentage[1];
            }
            std::cout << std::endl;
        }

        std::string data = std::string("SELECT id, title, album, artist, genre, duration, format, path_file FROM " + dbSchema + ".musics where artist ~ '") + arguments->artist + "'" ;
        std::cout << data << std::endl;
        std::vector<char> v(data.length() + 1);
        strcpy(&v[0], data.c_str());
        char* request = &v[0];
        requeteToBDD(request);

        std::vector<std::string> playlist_bruteDataLine = split(playlist_brute, '\n');
        for (int i=0; i < playlist_bruteDataLine.size(); i++)
        {
            std::vector<std::string> playlist_bruteDataCol = split(playlist_bruteDataLine[i], '|');
            for (int j=0; j < playlist_bruteDataCol.size(); j++)
            {
                if (j == 7)
                {
                    path_file_list.push_back(playlist_bruteDataCol[7]);
                }
            }
        }

        for (int i=0; i < path_file_list.size(); i++)
        {
            std::cout <<  path_file_list[i] << std::endl;
        }
        /*path_file_list.push(playlist_bruteData[7]);*/
        /*playgen::Artist* monartist = new playgen::Artist(std::stoi(playlist_bruteData[0]), playlist_bruteData[3], playlist_bruteData[3]);
        playgen::Album* monalbum = new playgen::Album(0,playlist_bruteData[2]);
        playlist_brutegen::Genre* mongenre = new playgen::Genre(0,playlist_bruteData[7]);
        monartist->print();
        monalbum->print();
        mongenre->print();*/
    }

    if (arguments->duration != "")
    {
        std::string data = std::string("SELECT title, album, artist, genre, duration, format, path_file FROM " + dbSchema + ".musics where duration = '") + arguments->duration + "'" ;
        std::cout << data << std::endl;
        std::vector<char> v(data.length() + 1);
        strcpy(&v[0], data.c_str());
        char* request = &v[0];
        requeteToBDD(request);
    }

    if (arguments->format != "")
    {
        std::string data = std::string("SELECT title, album, artist, genre, duration, format, path_file FROM " + dbSchema + ".musics where format ~ '") + arguments->format+ "'" ;
        std::cout << data << std::endl;
        std::vector<char> v(data.length() + 1);
        strcpy(&v[0], data.c_str());
        char* request = &v[0];
        requeteToBDD(request);
    }

    if (arguments->genre != "")
    {
        std::vector<std::string> genreData = split(arguments->genre, ',');
        for (int i=0; i < genreData.size(); i++)
        {
            std::vector<std::string> genrePourcentage = split(genreData[i], '=');
            std::cout << genrePourcentage[0];
            if (genrePourcentage.size() > 1)
            {
                std::cout << " - " << genrePourcentage[1];
            }
            std::cout << std::endl;
        }

        std::string data = std::string("SELECT title, album, artist, genre, duration, format, path_file FROM " + dbSchema + ".musics where genre ~ '") + arguments->genre + "'" ;
        std::cout << data << std::endl;
        std::vector<char> v(data.length() + 1);
        strcpy(&v[0], data.c_str());
        char* request = &v[0];
        requeteToBDD(request);
    }

    if (arguments->album != "")
    {
        std::vector<std::string> albumData = split(arguments->album, ',');
        for (int i=0; i < albumData.size(); i++)
        {
            std::vector<std::string> albumPourcentage = split(albumData[i], '=');
            std::cout << albumPourcentage[0];
            if (albumPourcentage.size() > 1)
            {
                std::cout << " - " << albumPourcentage[1];
            }
            std::cout << std::endl;
        }

        std::string data = std::string("SELECT title, album, artist, genre, duration, format, path_file FROM " + dbSchema + ".musics where album ~ '") + arguments->album + "'" ;
        std::cout << data << std::endl;
        std::vector<char> v(data.length() + 1);
        strcpy(&v[0], data.c_str());
        char* request = &v[0];
        requeteToBDD(request);
    }
}
