o "==== Installation of the database ===="
echo "--- Check if postgresql is running ---"
postgresql=$(systemctl status postgresql.service | cut -d'(' -f2 | cut -d')' -f1 | egrep "^(r|d)")
while [[ $postgresql == "dead"  ]]; do
      echo "Check postgresql [$postgresql]"
      echo "Starting postgresql... Your password can be asked."
      sudo systemctl start postgresql.service
      postgresql=$(systemctl status postgresql.service | cut -d'(' -f2 | cut -d')' -f1 | egrep "^(r|d)")
done
echo "Check postgresql [$postgresql]"
echo "--- Creation of the database 'playgen' in postgresql ---"
psql --username=postgres -c "DROP DATABASE \"playgen\";"
psql --username=postgres -c "CREATE DATABASE \"playgen\";"
echo "--- Creation of the role 'playgen' in postgresql ---"
echo " [!] You must set a password for the database : "
read pass
psql --username postgres -c "DROP USER playgen;"
psql --username postgres -c "CREATE USER playgen WITH PASSWORD '$pass';"
sed -i "s/^dbPass=.*$/dbPass=$pass/g" bin/bdd.conf
echo "--- Create schema 'playgen' in postgresql ---"
psql --username=postgres -d playgen -c "CREATE SCHEMA playgen AUTHORIZATION playgen"
echo "--- Create table 'musics' in postgresql ---"
psql --username=postgres -c "CREATE TABLE playgen.musics(ID SERIAL PRIMARY KEY NOT NULL,
TITLE varchar(255) NOT NULL,
ALBUM varchar(255) NOT NULL,
ARTIST varchar(255) NOT NULL,
GENRE varchar(255) NOT NULL,
SUBGENRE varchar(255) NOT NULL,
DURATION INT NOT NULL,
FORMAT varchar(255) NOT NULL,
POLYPHONY SMALLINT NOT NULL DEFAULT '2',
PATH_FILE varchar(255) NOT NULL);" -d playgen ;
psql --username postgres -d playgen -c "GRANT ALL PRIVILEGES ON playgen.musics TO playgen;"
echo "--- Add files in database ---"
echo "Adding file in silent... If you want to debug, edit the line 'psql -q --username=postgres -d playgen -f data.sql' and remove the '-q'."
psql -q --username=playgen -d playgen -f data.sql
