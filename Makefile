bin/playgen: obj/main.o obj/bdd.o obj/global.o obj/Artist.o obj/Album.o obj/Genre.o obj/QueryDB.o
	-mkdir bin
	g++ -std=c++11 $^ -o $@ -lpq
	@echo "==================================================="
	@echo "Compilation completed !"

obj/%.o: src/%.cpp
	-mkdir obj
	g++ -std=c++11 -c $< -o $@

build: 
	./.install.sh
deb:
	./.make_deb.sh
install:
	make deb
	sudo dpkg -i playgen.deb
