#!/bin/bash
test_libpq=$(dpkg-query -l libpq-dev | grep libpq | awk '{print $3}')
if [[ $test_libpq =~ "<" ]]; then
	echo "Le paquet libpq n'a pas été trouvé ! Installation du packet..."
	sudo apt-get update
	sudo apt-get install libpq-dev
	make bin/playgen
else
	make bin/playgen
fi

