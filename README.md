# PPE3_playlist

PPE3_playlist is the name of the project. This a school-project developped by Corentin Breton and Bastien Bidault.
The goal of the project is to develop a music playlist generator able to choose musics from a postgresql database.

The program is written in C++. It use libpq.

# playgen

playgen is the name of the program, developt to be a generator of m3u and xspf playlists.

It is a CLI program (Command Line Interface) aimed to generate a playlist with various caracteristics.
Like if you want playgen to select Rock musics , you can do :

```
playgen --genre Rock
```

And you playlist will be all Rock !

### Installation on Debian from a packet

[https://share.neodarz.ovh/playgen/playgen.deb](https://share.neodarz.ovh/playgen/playgen.deb)

### Installation on Debian from source

```
$ git clone https://framagit.org/Radio_SIO_playlist/playlist.git
$ make install
```

[get source](https://share.neodarz.ovh/playgen/playgen.tar.gz)

**developer note:** install_db.sh will install a database example (actually only supported on ArchLinux).

### Configuration

bdd.conf is used by the program to query the database.

You can configure your bdd.conf file in /etc/playgen/bdd.conf

```
dbHost= add your database address (ex: 127.0.0.1)
dbPort= add your database port (usually 5432)
dbName= add your database name
dbSchema= add your database schema
dbLogin= add your database login
dbPass= add the password for "login"
```

### Usage

```
$ playgen -a ArtistName PlaylistName
```

If you want to get more information you can try :

```
playgen --help
```
